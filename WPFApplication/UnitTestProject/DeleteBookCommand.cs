﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WPFApplication.ViewModels;

namespace UnitTestProject
{
    [TestClass]
    public class DeleteBookCommandTest
    {
        [TestMethod]
        public void DeleteCommand_Test01_CanExecute()
        {
            var mainVM = new MainViewModel();

            Assert.IsFalse(mainVM.DeleteCommand.CanExecute(mainVM),
                "Delete command cannot be executed if there are no books");

            mainVM.AddCommand.Execute(mainVM);
            Assert.AreEqual(1, mainVM.Library.Count);
            Assert.IsTrue(mainVM.DeleteCommand.CanExecute(mainVM));
        }

        [TestMethod]
        public void DeleteCommand_Test02_Execute()
        {
            var mainVM = new MainViewModel();
            mainVM.AddCommand.Execute(mainVM);
            Assert.AreEqual(1, mainVM.Library.Count);

            mainVM.DeleteCommand.Execute(mainVM);
            Assert.AreEqual(0, mainVM.Library.Count);
        }
    }
}
