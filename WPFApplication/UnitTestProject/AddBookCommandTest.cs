﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WPFApplication.ViewModels;

namespace UnitTestProject
{
    [TestClass]
    public class AddBookCommandTest
    {
        [TestMethod]
        public void AddCommand_Test01_CanExecute()
        {
            var mainVM = new MainViewModel();

            Assert.IsTrue(mainVM.AddCommand.CanExecute(mainVM),
                "Add command must be able to be executed even if there is no selected");
        }

        [TestMethod]
        public void AddCommand_Test02_Execute()
        {
            var mainVM = new MainViewModel();

            Assert.AreEqual(0, mainVM.Library.Count);

            mainVM.AddCommand.Execute(mainVM);
            Assert.AreEqual(1, mainVM.Library.Count);

            mainVM.AddCommand.Execute(mainVM);
            Assert.AreEqual(2, mainVM.Library.Count);
        }
    }
}
