﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using WPFApplication.Commands;

namespace WPFApplication.ViewModels
{
    public class MainViewModel : ViewModelBase
    {
        private int titleIndex = 0;
        private int authorIndex = 0;
        private BookViewModel selected = null;

        public MainViewModel()
        {
            this.Library = new ObservableCollection<BookViewModel>();
            this.LibraryFiltered = new ObservableCollection<BookViewModel>();
            this.TitleLibrary = new List<BrowserItem>();
            this.AuthorLibrary = new List<BrowserItem>();
            this.Library.CollectionChanged += Library_CollectionChanged;

            this.AddCommand = new AddBookCommand();
            this.EditCommand = new EditBookCommand();
            this.SaveCommand = new SaveBookCommand();
            this.DeleteCommand = new DeleteBookCommand();
            this.FilterCommand = new FilterBooksCommand();
        }
        
        public ObservableCollection<BookViewModel> Library
        {
            get;
            private set;
        }

        public List<BrowserItem> TitleLibrary
        {
            get;
            private set;
        }

        public List<BrowserItem> AuthorLibrary
        {
            get;
            private set;
        }

        public int TitleLibraryIndex
        {
            get
            {
                return this.titleIndex;
            }

            set
            {
                this.titleIndex = value;
                this.OnPropertyChanged();
                if (value < 0)
                    return;
                var title = this.TitleLibrary[value].Item;
                this.Selected = this.LibraryFiltered.First(b => b.Title == title);
            }
        }

        public int AuthorLibraryIndex
        {
            get
            {
                return this.authorIndex;
            }

            set
            {
                this.authorIndex = value;
                this.OnPropertyChanged();
                if (value < 0)
                    return;
                var author = this.AuthorLibrary[value].Item;
                this.Selected = this.LibraryFiltered.First(b => b.Author == author);
            }
        }

        public BookViewModel Selected
        {
            get
            {
                return this.selected;
            }
            set
            {
                if (this.selected != null)
                {
                    this.selected.IsReadOnly = true;
                }

                this.selected = value;
                this.RaiseCanExecuteChangedOnCommands();
                this.OnPropertyChanged();
            }
        }

        public ObservableCollection<BookViewModel> LibraryFiltered
        {
            get;
            private set;
        }

        public string Filter
        {
            get;
            set;
        }

        #region Commands
        public BaseCommand AddCommand
        {
            get;
            private set;
        }

        public BaseCommand EditCommand
        {
            get;
            private set;
        }

        public BaseCommand SaveCommand
        {
            get;
            private set;
        }

        public BaseCommand DeleteCommand
        {
            get;
            private set;
        }

        public BaseCommand FilterCommand
        {
            get;
            private set;
        }

        #endregion

        public void Initialize()
        {
            this.RaiseCanExecuteChangedOnCommands();
        }

        public void StartBookEdit()
        {
            this.Selected.CreateMemento();
            this.Selected.IsReadOnly = false;
            this.RaiseCanExecuteChangedOnCommands();
        }

        private void RaiseCanExecuteChangedOnCommands()
        {
            this.AddCommand.RaiseCanExecuteChanged();
            this.EditCommand.RaiseCanExecuteChanged();
            this.SaveCommand.RaiseCanExecuteChanged();
            this.DeleteCommand.RaiseCanExecuteChanged();
            this.FilterCommand.RaiseCanExecuteChanged();
        }

        public void ApplyLibraryFilter()
        {
            this.LibraryFiltered.Clear();
            var items = this.Library
                .Where(bookVM => string.IsNullOrWhiteSpace(this.Filter) ||
                    bookVM.Title.Contains(this.Filter) ||
                    bookVM.Author.Contains(this.Filter));

            foreach (var item in items) this.LibraryFiltered.Add(item);

            this.TitleLibrary =
                this.LibraryFiltered.GroupBy(book => book.Title)
                    .Select(titleBooks => new BrowserItem(titleBooks.Key, titleBooks.Count()))
                    .ToList();

            this.AuthorLibrary =
                this.LibraryFiltered.GroupBy(book => book.Author)
                    .Select(authorBooks => new BrowserItem(authorBooks.Key, authorBooks.Count()))
                    .ToList();

            this.Selected = this.LibraryFiltered.Count > 0 ?
                this.LibraryFiltered.First() : null;

            this.OnPropertyChanged("TitleLibrary");
            this.OnPropertyChanged("AuthorLibrary");
        }

        private void Library_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            this.Filter = string.Empty;
            this.OnPropertyChanged("Filter");
            this.ApplyLibraryFilter();
            this.RaiseCanExecuteChangedOnCommands();
        }
    }
}
