﻿using System;
using System.IO;
using System.Windows.Media.Imaging;
using WPFApplication.Models;

namespace WPFApplication.ViewModels
{
    public class BookViewModel : ViewModelBase
    {
        private bool isReadOnly;
        private Book model;
        private Book memento;

        public BookViewModel()
        {
            this.IsReadOnly = true;
            this.model = new Book()
            {
                Author = string.Empty,
                Title = string.Empty,
                ISBNnumber = string.Empty,
                CoverImagePath = string.Empty
            };
        }

        public BookViewModel(Book model)
        {
            this.IsReadOnly = true;
            this.model = model;
        }

        #region View Properties

        public bool IsReadOnly
        {
            get
            {
                return this.isReadOnly;
            }

            set
            {
                this.isReadOnly = value;
                if (this.isReadOnly) // if changes from editable to readonly restore the model to previous state.
                    this.RestoreMemento();
                this.OnPropertyChanged();
            }
        }

        #endregion

        #region Book Properties

        public string Title
        {
            get
            {
                return this.model.Title;
            }

            set
            {
                this.model.Title = value;
                this.OnPropertyChanged();
            }
        }

        public string Author
        {
            get
            {
                return this.model.Author;
            }

            set
            {
                this.model.Author = value;
                this.OnPropertyChanged();
            }
        }

        public string ISBN
        {
            get
            {
                return this.model.ISBNnumber;
            }

            set
            {
                this.model.ISBNnumber = value;
                this.OnPropertyChanged();
            }
        }

        public BitmapImage Cover
        {
            get
            {
                if (string.IsNullOrWhiteSpace(this.model.CoverImagePath))
                {
                    return null;
                }

                if (!File.Exists(string.Format("./images/{0}", this.model.CoverImagePath)))
                {
                    return null;
                }

                return new BitmapImage(new Uri(@"pack://siteoforigin:,,,/images/" + this.model.CoverImagePath, UriKind.Absolute));
            }
        }

        public string CoverImagePath
        {
            get
            {
                return this.model.CoverImagePath;
            }

            set
            {
                this.model.CoverImagePath = value;
                this.OnPropertyChanged();
            }
        }

        #endregion

        public void CreateMemento()
        {
            this.memento = model.Clone();
        }

        public void RestoreMemento()
        {
            if (this.memento == null)
                return;
            this.model = this.memento;
            this.memento = null;
        }

        public BookViewModel Clone()
        {
            return new BookViewModel(this.model.Clone());
        }
    }
}
