﻿namespace WPFApplication.ViewModels
{
    public class BrowserItem
    {
        public BrowserItem(string item, int count)
        {
            this.Item = item;
            this.Count = count;
        }

        public string Item
        {
            get;
            private set;
        }

        public int Count
        {
            get;
            private set;
        }

        public override string ToString()
        {
            if (this.Count > 1)
                return string.Format("{0} ({1})", this.Item, this.Count);

            return this.Item;
        }
    }
}
