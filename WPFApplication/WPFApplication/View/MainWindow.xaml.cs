﻿using System.Windows;
using WPFApplication.Models;
using WPFApplication.ViewModels;

namespace WPFApplication.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var viewModel = (MainViewModel)this.DataContext;
            viewModel.Library.Add(
                new BookViewModel(new Book()
                {
                    Title = "La magia del orden",
                    Author = "MARIE KONDO",
                    ISBNnumber = "8403501404",
                    CoverImagePath= "lamagiadelorden.jpg"
                }));
            viewModel.Library.Add(
                new BookViewModel(new Book()
                {
                    Title = "Destroza este diario",
                    Author = "Keri Smith",
                    ISBNnumber = "8449327857",
                    CoverImagePath = @"destrozaestediario.jpg"
                }));
            viewModel.Library.Add(
                new BookViewModel(new Book()
                {
                    Title = "Toca el piano",
                    Author = "James Rhodes",
                    ISBNnumber = "8416290571",
                    CoverImagePath = @"tocaelpiano.jpg"
                }));

            viewModel.Initialize();
        }
    }
}
