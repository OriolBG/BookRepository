﻿using WPFApplication.ViewModels;

namespace WPFApplication.Commands
{
    public class DeleteBookCommand : BaseCommand
    {
        public override void Execute(object parameter)
        {
            var mainVM = parameter as MainViewModel;

            if (mainVM.Selected != null && mainVM.Library.Contains(mainVM.Selected))
                mainVM.Library.Remove(mainVM.Selected);

            mainVM.Selected = mainVM.Library.Count > 0 ? mainVM.Library[0] : null;
        }
    }
}
