﻿using System;
using System.Windows.Input;
using WPFApplication.ViewModels;

namespace WPFApplication.Commands
{
    public abstract class BaseCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        public virtual bool CanExecute(object parameter)
        {
            var mainVM = parameter as MainViewModel;
            if (mainVM?.Selected == null)
                return false;

            return true;
        }

        public abstract void Execute(object parameter);

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, null);
        }
    }
}
