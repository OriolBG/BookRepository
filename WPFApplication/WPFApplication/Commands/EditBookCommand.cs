﻿using WPFApplication.ViewModels;

namespace WPFApplication.Commands
{
    public class EditBookCommand : BaseCommand
    {
        public override bool CanExecute(object parameter)
        {
            if (!base.CanExecute(parameter))
                return false;

            var mainVM = parameter as MainViewModel;
            return mainVM.Selected.IsReadOnly;
        }

        public override void Execute(object parameter)
        {
            var mainVM = parameter as MainViewModel;

            mainVM.StartBookEdit();
        }
    }
}
