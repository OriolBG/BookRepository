﻿using WPFApplication.Models;
using WPFApplication.ViewModels;

namespace WPFApplication.Commands
{
    public class AddBookCommand : BaseCommand
    {
        public readonly BookViewModel DefaultBook;

        public AddBookCommand()
        {
            this.DefaultBook = new BookViewModel(
                new Book()
                {
                    Title = "No title",
                    Author = "No author",
                    ISBNnumber = "0000",
                    CoverImagePath = "BookNotPictured.jpg"
                }
            );
        }

        public override bool CanExecute(object parameter)
        {
            return true;
        }

        public override void Execute(object parameter)
        {
            var mainVM = parameter as MainViewModel;

            var newBook = this.DefaultBook.Clone();
            mainVM.Library.Add(newBook);

            // Select it and start editing
            newBook.IsReadOnly = false;
            mainVM.Selected = newBook;
        }
    }
}
