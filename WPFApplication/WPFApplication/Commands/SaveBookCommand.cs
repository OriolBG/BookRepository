﻿using WPFApplication.ViewModels;

namespace WPFApplication.Commands
{
    public class SaveBookCommand : BaseCommand
    {
        public override bool CanExecute(object parameter)
        {
            if (!base.CanExecute(parameter))
                return false;

            var mainVM = parameter as MainViewModel;
            return !mainVM.Selected.IsReadOnly;
        }

        public override void Execute(object parameter)
        {
            var mainVM = parameter as MainViewModel;

            var i = mainVM.Library.IndexOf(mainVM.Selected);
            var modifiedBook = mainVM.Library[i].Clone();

            // replace the book in the same position by its clone so the list gets modified and the browsers are updated.
            mainVM.Library.RemoveAt(i);
            mainVM.Library.Insert(i, modifiedBook);

            // Select the new clone.
            mainVM.Selected = modifiedBook;
        }
    }
}
