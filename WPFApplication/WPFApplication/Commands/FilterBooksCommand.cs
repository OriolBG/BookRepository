﻿using WPFApplication.ViewModels;

namespace WPFApplication.Commands
{
    public class FilterBooksCommand : BaseCommand
    {
        public override bool CanExecute(object parameter)
        {
            return true;
        }

        public override void Execute(object parameter)
        {
            var mainVM = parameter as MainViewModel;
            mainVM.ApplyLibraryFilter();
        }
    }
}
