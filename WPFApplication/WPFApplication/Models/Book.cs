﻿namespace WPFApplication.Models
{
    public class Book
    {
        public string Title
        {
            get;
            set;
        }

        public string Author
        {
            get;
            set;
        }

        public string ISBNnumber
        {
            get;
            set;
        }

        public string CoverImagePath
        {
            get;
            set;
        }

        public Book Clone()
        {
            return new Book()
            {
                Title = this.Title,
                Author = this.Author,
                ISBNnumber = this.ISBNnumber,
                CoverImagePath = this.CoverImagePath
            };
        }
    }
}
