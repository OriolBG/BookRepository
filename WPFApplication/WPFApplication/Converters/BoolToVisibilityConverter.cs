﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace WPFApplication.Converters
{
    [ValueConversion(typeof(bool), typeof(Visibility))]
    public abstract class BoolToVisibilityConverter : IValueConverter
    {
        protected Visibility TrueValue { get; set; }
        protected Visibility FalseValue { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool))
                return null;
            return (bool)value ? TrueValue : FalseValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (Equals(value, TrueValue))
                return true;
            if (Equals(value, FalseValue))
                return false;
            return null;
        }
    }

}
