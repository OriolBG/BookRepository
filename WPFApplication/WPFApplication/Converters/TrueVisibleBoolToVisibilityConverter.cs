﻿using System.Windows;

namespace WPFApplication.Converters
{
    public class TrueVisibleBoolToVisibilityConverter : BoolToVisibilityConverter
    {
        public TrueVisibleBoolToVisibilityConverter()
        {
            this.TrueValue = Visibility.Visible;
            this.FalseValue = Visibility.Collapsed;
        }
    }
}
