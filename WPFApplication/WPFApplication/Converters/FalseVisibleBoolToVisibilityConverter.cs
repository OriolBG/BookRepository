﻿using System.Windows;

namespace WPFApplication.Converters
{
    public class FalseVisibleBoolToVisibilityConverter : BoolToVisibilityConverter
    {
        public FalseVisibleBoolToVisibilityConverter()
        {
            this.TrueValue = Visibility.Collapsed;
            this.FalseValue = Visibility.Visible;
        }
    }
}
